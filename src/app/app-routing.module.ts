import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CharitiesComponent} from './charities/charities.component'
import { HomeComponent } from './home/home.component';
import { CharityViewComponent } from './charity-view/charity-view.component';
import { LogInComponent } from './log-in/log-in.component';
import { RegistrationComponent } from './registration/registration.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', component:  HomeComponent},
  { path: 'charities', component: CharitiesComponent },
  { path: 'charities/:name', component: CharityViewComponent},
  { path: 'login', component: LogInComponent},
  { path: 'registration', component: RegistrationComponent},
  { path: 'home', component: HomeComponent},
  { path: 'profile', component: ProfilePageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
