import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharitySearchComponent } from './charity-search.component';

describe('CharitySearchComponent', () => {
  let component: CharitySearchComponent;
  let fixture: ComponentFixture<CharitySearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharitySearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharitySearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
