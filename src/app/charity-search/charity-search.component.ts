import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Charity } from '../interfaces/charity';
import { CharityService } from '../services/charity.service';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-charity-search',
  templateUrl: './charity-search.component.html',
  styleUrls: ['./charity-search.component.css']
})
export class CharitySearchComponent implements OnInit {
  charities$: Observable<Charity[]>;
  private searchTerms = new Subject<string>();
  
  constructor(private charityService: CharityService) { }

  search(term: string): void {
    this.searchTerms.next(term);
  }

  ngOnInit(): void {
    this.charities$ = this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),

      // ignore new term if same as previous term
      distinctUntilChanged(),

      // switch to new search observable each time the term changes
      switchMap((term: string) => this.charityService.searchCharities(term)),
    );
  }

}
