import { Component, OnInit, Input } from '@angular/core';
import { CharityService } from '../services/charity.service';
import { Charity } from '../interfaces/charity';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Participation } from '../interfaces/participation';
import { DonationService } from '../services/donation.service';
import { ParticipationService } from '../services/participation.service';
import { AppService } from '../services/app.service';
import { Donation } from '../interfaces/donation';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-charity-view',
  templateUrl: './charity-view.component.html',
  styleUrls: ['./charity-view.component.css']
})
export class CharityViewComponent implements OnInit {

  charity: Charity;
  collectedParticipants: number;
  collectedAmount: number;
  estimationAmount: number = 0;
  confirmationDialogDonation: boolean = true;
  confirmationDialogParticipation: boolean = true;
  checkUserIsParticipantForCharity: boolean = false;
  constructor(private charityService: CharityService, 
              private route: ActivatedRoute, 
              private donationService: DonationService,
              private participationsService: ParticipationService,
              private appService: AppService,
              private router: Router,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    this.getCharity();
  }

  getCharity(): void {
    const name = this.route.snapshot.paramMap.get('name');
    this.charityService.getCharity(name)
      .subscribe(charity => {
        if(charity.name){
          this.charity = charity;
          this.getCollectedAmountForCharity();
          this.getCollectedParticipationsForCharity();
          this.checkUserIsParticipantForCharityF();
          this.estimation();
        }
      });
  }
   
  getCollectedAmountForCharity(){
    this.donationService.getCollectedAmountForCharity(this.charity.name).subscribe(value => {
      this.collectedAmount=value;
    });
  }

  getCollectedParticipationsForCharity(){
    this.participationsService.getCollectedParticipationsForCharity(this.charity.name).subscribe(value =>{
      this.collectedParticipants = value;
    });
  }
  
  makeDonation(amount: number){
    if(amount <= 0) {return;}
    const nameDonator = this.appService.getCurrentUserName();
    this.confirmationDialogDonation = true;
    const date = '';
    const nameCharity = this.charity.name;
    this.donationService.addDonation({nameDonator,nameCharity, amount, date} as Donation).subscribe();
    this.getCharity();
    this.estimation();
  }

  makeParticipant(){
    if(!this.checkUserIsParticipantForCharity ){
      const nameParticipant = this.appService.getCurrentUserName();
      this.confirmationDialogParticipation = true;
      const date = '';
      const nameCharity = this.charity.name;
      this.participationsService.addParticipation({nameParticipant,nameCharity,date} as Participation).subscribe();
      this.checkUserIsParticipantForCharity = true;
      this.getCharity();
    }else{
      this.toastr.error("You are already a participant.");
      this.closeConfirmationDialogParticipation();
    }
  }

  checkCurrentUserIsCharityCreator(){
    return this.charity.nameCreator === this.appService.getCurrentUserName();
  }

  deleteCharity(){
    this.charityService.deleteCharity(this.charity.name).subscribe(); //return money
    this.router.navigate(['']);
  }

  openConfirmationDialogDonation(){
    const nameParticipant = this.appService.getCurrentUserName();
    if(nameParticipant){
    this.confirmationDialogDonation = false;
    }else{
      this.router.navigate(['/login']);
    }
  }

  closeConfirmationDialogDonation(){
    this.confirmationDialogDonation = true;
  }

  openConfirmationDialogParticipation(){
    const nameParticipant = this.appService.getCurrentUserName();
    if(nameParticipant){
    this.confirmationDialogParticipation = false;
    }else{
      this.router.navigate(['/login']);
    }
  }

  closeConfirmationDialogParticipation(){
    this.confirmationDialogParticipation = true;
  }

  checkUserIsParticipantForCharityF(){
    const nameParticipant = this.appService.getCurrentUserName();
    this.participationsService.checkUserIsParticipantForCharity(nameParticipant,this.charity.name).subscribe(
      value => {
        this.checkUserIsParticipantForCharity = value;
      });
  }

  estimation(){
    let count = 0;
    this.donationService.getAllDonationsByCharity(this.charity.name).subscribe(currentCharity =>{
      count++;
    });
    if(count > 0){
        this.estimationAmount = this.collectedAmount / count;
    }
  }
}
