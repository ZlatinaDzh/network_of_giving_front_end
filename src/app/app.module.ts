import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CharitiesComponent } from './charities/charities.component';
import { ClarityModule } from '@clr/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CharityViewComponent } from './charity-view/charity-view.component';
import { CharitySearchComponent } from './charity-search/charity-search.component';
import { RegistrationComponent } from './registration/registration.component';
import { LogInComponent } from './log-in/log-in.component';
import { FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { EditCharityPageComponent } from './edit-charity-page/edit-charity-page.component';
@NgModule({
  declarations: [
    AppComponent,
    CharitiesComponent,
    HomeComponent,
    CharityViewComponent,
    CharitySearchComponent,
    RegistrationComponent,
    LogInComponent,
    ProfilePageComponent,
    EditCharityPageComponent 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ClarityModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    FormsModule,
	  ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
