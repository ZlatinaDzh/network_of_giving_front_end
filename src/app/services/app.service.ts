import { Injectable } from '@angular/core'
import { Observable } from 'rxjs';
@Injectable ({ providedIn: 'root'})
export class AppService{
    private currentUser:string;
    private isAuthenticated:boolean = false;

    constructor(){}

    logIn(userName:string){
        this.currentUser = userName;
        this.isAuthenticated = true;
    }

    logOut(){
        this.currentUser = null;
        this.isAuthenticated = false;
    }

    checkIsAuthenticated(){
        return this.isAuthenticated;
    }

    getCurrentUserName(){
        return this.currentUser;
    }

}