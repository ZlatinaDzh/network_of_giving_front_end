import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Participation } from '../interfaces/participation';
import { Credentials } from '../interfaces/credentials';
import { HandleErrorService } from './handleErrorService';

@Injectable ({ providedIn: 'root'})
export class ParticipationService{
  
    private participationsUrl = 'http://localhost:8080/participations'; // url to web

    httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    };

    constructor(
        private http: HttpClient,
        private handleErrorService: HandleErrorService) { }


    getParticipations (): Observable<Participation[]> {
        return this.http.get<Participation[]>(this.participationsUrl)
        .pipe(
            tap(_ => console.log('fetched participations')),
            catchError(this.handleErrorService.handleError<Participation[]>('getParticipations', []))
        );
    }

    getParticipation(id: number): Observable<Participation>{
        const url = `${this.participationsUrl}/${id}`;
        return this.http.get<Participation>(url).pipe(
          tap(_ => console.log(`fetched participation id=${id}`)),
          catchError(this.handleErrorService.handleError<Participation>(`getParticipation id=${id}`))
        );
    }

 
    addParticipation(participation: Participation): Observable<Participation>{
        return this.http.post<Participation>(this.participationsUrl, participation, this.httpOptions);
        // .pipe(
        //     tap((newUser : User) => this.log(`added user w/ name=${newUser.userName}`)),
        //     catchError(this.handleError<User>(`addUser`))
        // );
    }

    
    updatePartcipation(participation : Participation): Observable<any>{
        return this.http.put(this.participationsUrl, participation, this.httpOptions)
        .pipe(
            tap(_ => console.log(`update participation id=${participation.id}`)),
            catchError(this.handleErrorService.handleError<any>('updateParticipation'))
        );
    }


    deleteParticipation(participation: Participation | number): Observable<Participation>{
        const id = typeof participation === 'number' ? participation : participation.id;
        const url = `${this.participationsUrl}/${id}`;

        return this.http.delete<Participation> (url, this.httpOptions)
        .pipe(
            tap(_ => console.log(`deleted participation id=${id}`)),
        );
    }

   
    getAllParticipationsByUser(username: string): Observable<Participation[]>{
        const url = `${this.participationsUrl}/donators/${username}`;
        return this.http.get<Participation[]>(url).pipe(
            tap(_ => console.log('fetched participations')),
            catchError(this.handleErrorService.handleError<Participation[]>('getAllParticipationsByUser', []))
        );  
    }

    getAllParticipationsByCharity(charityName: string): Observable<Participation[]>{
        const url = `${this.participationsUrl}/charities/${charityName}`;
        return this.http.get<Participation[]>(url).pipe(
            tap(_ => console.log('fetched participations')),
            catchError(this.handleErrorService.handleError<Participation[]>('getAllParticipationsByCharity', []))
        );  
    }

    getCollectedParticipationsForCharity(charityName: string): Observable<number>{
        const url = `${this.participationsUrl}/for/${charityName}`;
        return this.http.get<number>(url);
    }
    
    checkUserIsParticipantForCharity(username:string, charityName:string): Observable<boolean>{
        const url = `${this.participationsUrl}/checkParticipant`;
        const credentials = new Credentials();
        credentials.firstAttribute = username;
        credentials.secondAttribute = charityName;
        return this.http.post<boolean>(url,credentials,this.httpOptions);
    }

}