import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Donation } from '../interfaces/donation';
import { HandleErrorService } from './handleErrorService';

@Injectable ({ providedIn: 'root'})
export class DonationService{
  
    private donationsUrl = 'http://localhost:8080/donations'; // url to web

    httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    };

    constructor(
        private http: HttpClient,
        private handleErrorService:HandleErrorService) { }


    getDonations (): Observable<Donation[]> {
        return this.http.get<Donation[]>(this.donationsUrl)
        .pipe(
            tap(_ => console.log('fetched donations')),
            catchError(this.handleErrorService.handleError<Donation[]>('getDonations', []))
        );
    }


    getDonation(id: number): Observable<Donation>{
        const url = `${this.donationsUrl}/${id}`;
        return this.http.get<Donation>(url).pipe(
          tap(_ => console.log(`fetched donation id=${id}`)),
          catchError(this.handleErrorService.handleError<Donation>(`getDonation id=${id}`))
        );
    }


    addDonation(donation: Donation): Observable<Donation>{
        return this.http.post<Donation>(this.donationsUrl, donation, this.httpOptions);
        // .pipe(
        //     tap((newUser : User) => this.log(`added user w/ name=${newUser.userName}`)),
        //     catchError(this.handleError<User>(`addUser`))
        // );
    }

    updateDonation(donation : Donation): Observable<any>{
        return this.http.put(this.donationsUrl, donation, this.httpOptions)
        .pipe(
            tap(_ => console.log(`update donation id=${donation.id}`)),
            catchError(this.handleErrorService.handleError<any>('updateDonation'))
        );
    }

    deleteDonation(donation: Donation | number): Observable<Donation>{
        const id = typeof donation === 'number' ? donation : donation.id;
        const url = `${this.donationsUrl}/${id}`;

        return this.http.delete<Donation> (url, this.httpOptions)
        .pipe(
            tap(_ => console.log(`deleted donation id=${id}`)),
        );
    }

   
    getAllDonationsByUser(username: string): Observable<Donation[]>{
        const url = `${this.donationsUrl}/donators/${username}`;
        return this.http.get<Donation[]>(url).pipe(
            tap(_ => console.log('fetched donations')),
            catchError(this.handleErrorService.handleError<Donation[]>('getAllDonationsByUser', []))
        );  
    }

    getAllDonationsByCharity(charityName: string): Observable<Donation[]>{
        const url = `${this.donationsUrl}/charities/${charityName}`;
        return this.http.get<Donation[]>(url).pipe(
            tap(_ => console.log('fetched donations')),
            catchError(this.handleErrorService.handleError<Donation[]>('getAllDonationsByCharity', []))
        );  
    }

    getCollectedAmountForCharity(charityName: string): Observable<number>{
        const url = `${this.donationsUrl}/for/${charityName}`;
        return this.http.get<number>(url);
    }
    
}