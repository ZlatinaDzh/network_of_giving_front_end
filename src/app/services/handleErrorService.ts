import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class HandleErrorService {
  constructor( private toastr: ToastrService) { }
  handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      if (error.error) {
        this.toastr.error(error.error.message);
        console.error(error.error.message); // log to console instead
      } else {
        // TODO: better job of transforming error for user consumption
        console.log(`${operation} failed: ${error.message}`);
      }
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}