import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { User } from '../interfaces/user';
import { Credentials } from '../interfaces/credentials';
import { UserDTO } from '../interfaces/userDTO';
import { HandleErrorService } from './handleErrorService';

@Injectable ({ providedIn: 'root'})
export class UserService{
  
    private usersUrl = 'http://localhost:8080/users'; // url to web

    httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    };

    constructor(
        private http: HttpClient,
        private handleErrorService: HandleErrorService) { }

    getUsers (): Observable<User[]> {
        return this.http.get<User[]>(this.usersUrl)
        .pipe(
            tap(_ => console.log('fetched users')),
            catchError(this.handleErrorService.handleError<User[]>('getUsers', []))
        );
    }

    getUser(name: string): Observable<User>{
        const url = `${this.usersUrl}/${name}`;
        return this.http.get<User>(url).pipe(
          tap(_ => console.log(`fetched user name=${name}`)),
          catchError(this.handleErrorService.handleError<User>(`getUser name=${name}`))
        );
    }

    addUser(user: UserDTO): Observable<boolean>{
        return this.http.post<boolean>(this.usersUrl, user, this.httpOptions)
        .pipe(
            catchError(this.handleErrorService.handleError<boolean>(`addUser`, false))
        );
    }

    updateUser(user : User): Observable<any>{
        return this.http.put(this.usersUrl, user, this.httpOptions)
        .pipe(
            tap(_ => console.log(`updated user name=${user.userName}`)),
            catchError(this.handleErrorService.handleError<any>('updateUser'))
        );
    }

    deleteUser(user: User | string): Observable<User>{
        const name = typeof user === 'string' ? user : user.userName;
        const url = `${this.usersUrl}/${name}`;

        return this.http.delete<User> (url, this.httpOptions)
        .pipe(
            tap(_ => console.log(`deleted user name=${name}`)),
            catchError(this.handleErrorService.handleError<any>('deleteUser'))
        );
    }

    logInUser(userName:string, password:string): Observable<boolean>{
        const url = `${this.usersUrl}/login`;
        const credentials = new Credentials();
        credentials.firstAttribute = userName;
        credentials.secondAttribute = password;
        return this.http.post<boolean>(url,credentials,this.httpOptions);
    }
    
    checkIfUserExist(username:string): Observable<boolean>{
        const url = `${this.usersUrl}/checkUsername`;
        return this.http.post<boolean>(url,username,this.httpOptions);
    }

}