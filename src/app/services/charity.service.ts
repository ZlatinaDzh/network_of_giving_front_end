import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Charity } from '../interfaces/charity';
import { HandleErrorService } from './handleErrorService';

@Injectable ({ providedIn: 'root'})
export class CharityService{
  
    private charitiesUrl = 'http://localhost:8080/charities'; // url to web

    httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    };

    constructor(
        private http: HttpClient,
        private handleErrorService: HandleErrorService) { }

    getCharities(): Observable<Charity[]> {
        return this.http.get<Charity[]>(this.charitiesUrl)
        .pipe(
            tap(_ => console.log('fetched charities')),
            catchError(this.handleErrorService.handleError<Charity[]>('getCharities', []))
        );
    }

  
    getCharity(name: string): Observable<Charity>{
        const url = `${this.charitiesUrl}/${name}`;
        return this.http.get<Charity>(url).pipe(
          tap(_ => console.log(`fetched charity name=${name}`)),
          catchError(this.handleErrorService.handleError<Charity>(`getCharity name=${name}`))
        );
    }


    addCharity(charity: Charity): Observable<boolean>{
        return this.http.post<boolean>(this.charitiesUrl, charity, this.httpOptions)
        .pipe(
            catchError(this.handleErrorService.handleError<boolean>(`addCharity`, false))
        );
    }

 
    updateCharity(currentName:string, charity : Charity): Observable<any>{
        return this.http.put(this.charitiesUrl, {charityName: currentName,charity: charity}, this.httpOptions)
        .pipe(
            tap(_ => console.log(`updated charity name=${charity.name}`)),
            catchError(this.handleErrorService.handleError<any>('updateCharity'))
        );
    }


    deleteCharity(charity: Charity | string): Observable<Charity>{
        const name = typeof charity === 'string' ? charity : charity.name;
        const url = `${this.charitiesUrl}/${name}`;
        return this.http.delete<Charity> (url, this.httpOptions)
        .pipe(
            tap(_ => console.log(`deleted charity name=${name}`)),
        );
    }

 
    searchCharities(term: string): Observable<Charity[]> {
      if (!term.trim()) {
        return of([]);
      }
      return this.http.get<Charity[]>(`${this.charitiesUrl}/?name=${term}`).pipe(
        tap(x => x.length ?
           console.log(`found charities matching "${term}"`) :
           console.log(`no charities matching "${term}"`)),
        catchError(this.handleErrorService.handleError<Charity[]>('searchCharities', []))
      );
    }

    checkIfCharityExist(charityName:string): Observable<boolean>{
        const url = `${this.charitiesUrl}/checkCharityName`;
        return this.http.post<boolean>(url,charityName,this.httpOptions);
    }
}