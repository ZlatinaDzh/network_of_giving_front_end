import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Address } from '../interfaces/address';
import { HandleErrorService } from './handleErrorService';

@Injectable ({ providedIn: 'root'})
export class AddressService{
  
    private addressesUrl = 'http://localhost:8080/addresses'; // url to web

    httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    };

    constructor(
        private http: HttpClient,
        private handleErrorService: HandleErrorService) { }


    getAddresses(): Observable<Address[]> {
        return this.http.get<Address[]>(this.addressesUrl)
        .pipe(
            tap(_ => console.log('fetched addresses')),
            catchError(this.handleErrorService.handleError<Address[]>('geAddresses', []))
        );
    }


    getAddress(id: number): Observable<Address>{
        const url = `${this.addressesUrl}/${id}`;
        return this.http.get<Address>(url).pipe(
          tap(_ => console.log(`fetched address id=${id}`)),
          catchError(this.handleErrorService.handleError<Address>(`getAddress id=${id}`))
        );
    }


    addAddress(address: Address): Observable<number>{
        return this.http.post<number>(this.addressesUrl, address, this.httpOptions);
        // .pipe(
        //     tap((newAddress : Address) => this.log(`added address w/ id=${newAddress.id}`)),
        //     catchError(this.handleError<Address>(`addAddress`))
        // );
    }

   
    updateAddress(address: Address): Observable<any>{
        return this.http.put(this.addressesUrl, address, this.httpOptions)
        .pipe(
            tap(_ => console.log(`updated address id=${address.id}`)),
            catchError(this.handleErrorService.handleError<any>('updateAddress'))
        );
    }

 
    deleteAddress(address: Address | number): Observable<Address>{
        const id = typeof address === 'number' ? address : address.id;
        const url = `${this.addressesUrl}/${id}`;

        return this.http.delete<Address> (url, this.httpOptions)
        .pipe(
            tap(_ => console.log(`deleted address id=${id}`)),
        );
    }
    
}