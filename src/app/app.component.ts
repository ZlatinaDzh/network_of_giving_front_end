import { Component } from '@angular/core';
import { AppService } from './services/app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'network-of-giving';
  confirmationDialog: boolean = true;

  constructor(private appService: AppService){}

  isUserAuthenticated(){
    return this.appService.checkIsAuthenticated();
  }

  getCurrentUsername(){
    return this.appService.getCurrentUserName();
  }

  logOut(){
    this.appService.logOut();
    this.confirmationDialog = true;
  }

  openConfirmationDialog(){
    this.confirmationDialog = false;
  }

  closeConfirmationDialog(){
    this.confirmationDialog = true;
  }
}
