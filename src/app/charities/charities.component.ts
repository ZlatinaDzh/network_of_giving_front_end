import { Component, OnInit } from '@angular/core';
import { CharityService } from '../services/charity.service'
import { Charity } from '../interfaces/charity'
import { AppService } from '../services/app.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-charities',
  templateUrl: './charities.component.html',
  styleUrls: ['./charities.component.css']
})
export class CharitiesComponent implements OnInit {
  confirmationDialog: boolean = true;
  picture: string;
  constructor(private charityService: CharityService,
              private appService: AppService,
              private toastr: ToastrService,
              private router: Router) { }

  ngOnInit(): void {
  }


  addCharity(name: string, description: string,
             numberOfParticipants: number, requiredBudget: number,
             nameCreator: string): void {

      this.closeConfirmationDialog();

      name = name.trim();
      if(!name) {
        this.toastr.error("The charity must have a name !");
        return;
      }
      
      if(numberOfParticipants > 0 || requiredBudget > 0){
        if(!this.appService.getCurrentUserName()) {
           this.toastr.error("Please log in or make registration!") 
           this.router.navigate(['/login']); 
           return;
        }
        const thumbnail = this.picture;
        this.charityService.addCharity({ name, thumbnail, description, numberOfParticipants,
                                          requiredBudget, nameCreator} as Charity).subscribe( value => {
              if(value){
                this.router.navigate([`/charities/${name}`]);
               }
          });
       }else{
         this.toastr.error("Тhe charity must have a budget or volunteers!");
       }
    }
    
  getUsername(){
    return this.appService.getCurrentUserName();
  }

  openConfirmationDialog(){
    this.confirmationDialog = false;
  }

  closeConfirmationDialog(){
    this.confirmationDialog = true;
  }

  onFileChanged(event) {
    if (event.target.files.length > 0) {
      console.log(event.target.files[0].name);
      this.picture = event.target.files[0].name;
    }
  }
}
