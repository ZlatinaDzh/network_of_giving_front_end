import { Component, OnInit } from '@angular/core';
import { Charity } from '../interfaces/charity';
import { CharityService } from '../services/charity.service';
import { AppService } from '../services/app.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  charities: Charity[];
  
  constructor(private charityService: CharityService,
              private appService : AppService) { }
  
  ngOnInit(): void {
     this.getCharities();
  }

  getCharities(): void{
    this.charityService.getCharities()
    .subscribe(charities => this.charities = charities);
  }

  isUserAuthenticated(){
    return this.appService.checkIsAuthenticated();
  }

  getCurrentUsername(){
    return this.appService.getCurrentUserName();
  }

  lineRange(value:string){
    const lines = value.split(".");
    const start: number = 0;
    const end: number = ((lines.length > 1) ? 1 : lines.length) as number;
    return lines.slice(start, end).join(".");
  }
}
