import { Gender } from './gender';

export interface UserDTO{
    firstName: string;
    lastName: string;
    userName: string;
    password: string;
    age: number;
    gender: Gender;
    country: string,
    town: string,
    neighbourhood: string,
    street: string,
    apartmentNumber: number
}