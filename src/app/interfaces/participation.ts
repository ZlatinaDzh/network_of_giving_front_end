export interface Participation{
    id: number;
    nameParticipant: string;
    nameCharity: string;
    date: string;
}