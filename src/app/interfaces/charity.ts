export interface Charity{
    name: string,
    thumbnail: string,
    description: string,
    numberOfParticipants: number,
    requiredBudget: number,
    nameCreator: string
}