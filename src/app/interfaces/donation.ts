export interface Donation{
    id: number,
    nameDonator: string,
    nameCharity: string,
    amount: number,
    date: string
}