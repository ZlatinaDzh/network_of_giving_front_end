import { Gender } from './gender';

export interface User{
    firstName: string;
    lastName: string;
    userName: string;
    password: string;
    age: number;
    gender: Gender;
}