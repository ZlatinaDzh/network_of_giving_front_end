export interface Address{
    id: number,
    country: string,
    town: string,
    neighbourhood: string,
    street: string,
    apartmentNumber: number
}