import { Component, OnInit } from '@angular/core';
import { AppService } from '../services/app.service';
import { UserService } from '../services/user.service';
import { AddressService } from '../services/address.service';
import { Gender } from '../interfaces/gender';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { UserDTO } from '../interfaces/userDTO';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  constructor(private appService: AppService,
              private userService: UserService,
              private addressService: AddressService,
              private toastr: ToastrService,
              private router: Router) { }

  ngOnInit(): void {
  }

  addUser(firstName: string, lastName: string, userName: string, password: string, age: number,
          country: string, town: string, neighbourhood: string,street: string,apartmentNumber: number):void{
                const gender = Gender.FEMALE;
                this.userService.addUser({firstName,lastName,userName,password,age,gender,country,town, neighbourhood,
                                          street,apartmentNumber} as UserDTO).subscribe( value => {
                                            console.log(value);
                                            if(value){
                                              this.router.navigate(['/login']);
                                            }
                                          });
           }
}
