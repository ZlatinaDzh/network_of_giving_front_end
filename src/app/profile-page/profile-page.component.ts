import { Component, OnInit } from '@angular/core';
import { AppService } from '../services/app.service';
import { UserService } from '../services/user.service';
import { User } from '../interfaces/user';
import { Charity } from '../interfaces/charity';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css']
})
export class ProfilePageComponent implements OnInit {
  user: User;
  charities: Charity[];
  constructor(private appService: AppService,
              private userService: UserService) { }

  ngOnInit(): void {
    this.getUser();
  }

  getUser(){
    const username = this.appService.getCurrentUserName();
    this.userService.getUser(username).subscribe(user => {
      if(user.userName){
        this.user= user;
      }
    });
  }

  
}
