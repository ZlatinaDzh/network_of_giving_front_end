import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { AppService } from '../services/app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {
  checkUser : boolean = true;
  username:string;
  password:string;
  constructor(private userService : UserService,
              private appService :  AppService,
              private router: Router) { }

  ngOnInit(): void {
  }
  logIn(username:string, password:string){
    this.username = username;
    this.password = password;
    this.userService.checkIfUserExist(username).subscribe(result =>{
      if(result){
        this.userService.logInUser(this.username, this.password).subscribe( result2 => {
          if(result2){
            this.appService.logIn(this.username);
            this.router.navigate(['']);
          }else{
            this.checkUser = false;
          }
        }
        )
      }else{
        this.checkUser = false;
      }
    });
  }

  checkUsenameExist(username:string){
    this.userService.checkIfUserExist(username).subscribe(result =>{
      return result;
    })
  }
}
